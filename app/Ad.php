<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ad extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'title', 'author_name', 'description', 'email',
    ];
}
