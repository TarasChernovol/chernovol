<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ad;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class AdController extends Controller
{
    /**
     * adController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $adList = Ad::paginate(5);

        return view('ad.index', compact('adList'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function add()
    {
        return view('ad.add');
    }

    /**
     * @param \App\Ad $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function edit(Ad $id)
    {
        $ownerEmail = DB::table('ads')->where('id', $id['id'])->value('email');
        $currentEmail = Auth::user()['email'];

        $ad = $id;

        if ($ownerEmail == $currentEmail) {
            return view('ad.edit', compact('ad'));
        } else {
            flash('Sorry. Only ' . '<strong>' . $ownerEmail . '</strong>' . ' can do this.', 'warning');

            return redirect()->route('ad.index');
        }
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function save(Request $request)
    {
        $request->validate([
            'title' => 'required|max:255',
            'author_name' => 'required|max:255',
            'description' => 'required',
        ]);

        $data = $request->all() + ['email' => Auth::user()['email']];

        $ad = Ad::create($data);
        flash('<strong>' . $ad->title . '</strong>' . ' is saved.', 'success');

        return redirect()->route('ad.index');
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Ad $id
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, Ad $id)
    {
        $request->validate([
            'title' => 'required|max:255',
            'author_name' => 'required|max:255',
            'description' => 'required',
        ]);

        $id->update($request->all());
        flash('<strong>' . $id->title . '</strong>' . ' is updated.', 'success');

        return redirect()->route('ad.index');
    }

    /**
     * @param \App\Ad $id
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Ad $id)
    {
        $ownerEmail = DB::table('ads')->where('id', $id['id'])->value('email');
        $currentEmail = Auth::user()['email'];

        if ($ownerEmail == $currentEmail) {
            $id->delete();
            flash('<strong>' . $id->title . '</strong>' . ' is deleted.', 'success');

        } else {
            flash('Sorry. Only ' . '<strong>' . $ownerEmail . '</strong>' . ' can do this.', 'warning');

            return redirect()->route('ad.index');
        }

        return redirect()->route('ad.index');
    }
}