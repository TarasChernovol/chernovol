<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ad;

class HomeController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $adList = Ad::paginate(5);

        return view('home', compact('adList'));
    }

    public function showAd($id)
    {
        return view('showAd', ['ad' => Ad::findOrFail($id),]);
    }
}