@extends('layouts.app')
@section('content')
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    {!! Form::open(['route' => 'ad.save', 'class' => 'form-horizontal form-label-left', 'data-parsley-validate']) !!}
    <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="title">Title
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
            {!! Form::text('title', null, ['class' => 'form-control col-md-7 col-xs-12', 'id' => 'title']) !!}
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="author_name">Author
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
            {!! Form::text('author_name', null, ['class' => 'form-control col-md-7 col-xs-12', 'id' => 'author_name']) !!}
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="description">Description
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
            {!! Form::textarea('description', null, ['class' => 'form-control col-md-7 col-xs-12', 'id' => 'description']) !!}
        </div>
    </div>
    <div class="ln_solid"></div>
    <div class="form-group">
        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
            <a href="{{ route('ad.index') }}" type="button" class="btn btn-primary">Cancel</a>
            {!! Form::submit('Create Ad', ['class' => 'btn btn-success']) !!}
        </div>
    </div>
    {!! Form::close() !!}
@endsection