@extends('layouts.app')
@section('content')
    @include('flash::message')
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Ad List
                </div>
                <div class="panel-body">
                    <a style="margin-bottom: 15px" href="{{ route('ad.add') }}" type="button"
                            class="btn btn-sm btn-success">Create Ad</a>
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th style="width: 5%">ID</th>
                                    <th style="width: 15%">Title</th>
                                    <th style="width: 25%">Author</th>
                                    <th style="width: 5%">Timestamp</th>
                                    <th>Description</th>
                                    <th style="width: 5%">Created by</th>
                                    <th style="width: 7.7%"></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($adList as $ad)
                                    <tr>
                                        <td>{{$ad->id?$ad->id:''}}</td>
                                        <td>{{$ad->title?$ad->title:''}}</td>
                                        <td>{{$ad->author_name?$ad->author_name:''}}</td>
                                        <td>{{$ad->creation_date?$ad->creation_date:''}}</td>
                                        <td>{{$ad->description?$ad->description:''}}</td>
                                        <td>{{$ad->email?$ad->email:''}}</td>
                                        <td>
                                            <a href="{{route('ad.edit', [$ad->id])}}" title="Edit"
                                                    class="btn btn-default btn-sm"><i class="fa fa-pencil"></i></a>
                                            <a type="button" class="btn btn-default btn-sm" title="Delete"
                                                    data-toggle="modal" data-target="#deleteModal{{ $ad->id }}"><i
                                                        class="fa fa-trash"></i></a>
                                            <div id="deleteModal{{ $ad->id }}" class="modal fade" role="dialog">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal"
                                                                    aria-label="Close"><span
                                                                        aria-hidden="true">&times;</span>
                                                            </button>
                                                            <h4 class="modal-title">Confirm Delete</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <p>Are you sure you want to delete this item?</p>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-default pull-left"
                                                                    data-dismiss="modal">Close
                                                            </button>
                                                            {!! Form::open(['route' => ['ad.destroy', $ad->id],
                                                            'method' => 'delete']) !!}
                                                            {!! Form::submit('Yes', ['class' => 'btn btn-success
                                                            btn-flat']) !!}
                                                            {!! Form::close() !!}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <span class="pull-left">{!! $adList->setPath('')->appends(Input::query())->render()
                                !!}</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection