@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        @if(Auth::check() == 1)
                            <a title="Edit" class="ad-edit" href="{{ url('ad/' . (int)$ad->id) }}"><span
                                        class="fa fa-pencil" aria-hidden="true"></span></a>
                        @endif
                        <a style="font-size: 22px" href="{{ $ad->id  }}">{{$ad->title?$ad->title:''}}</a>
                        <h4>
                            <span style="color: #777777">Author:</span><br>{{$ad->author_name?$ad->author_name:''}}
                        </h4>
                        <h4><span style="color: #777777">Creation
                                date:</span><br>{{$ad->creation_date?$ad->creation_date:''}}</h4>
                        <h4>
                            <span style="color: #777777">Description:</span><br>{{$ad->description?$ad->description:''}}
                        </h4>
                        <h4>
                            <span style="color: #777777">Email:</span><br>{{$ad->email?$ad->email:''}}
                        </h4>
                    </div>
                </div>
            </div>
        </div>
        <a href="{{ url('/') }}" type="button" class="btn btn-primary">Back</a>
    </div>
@endsection