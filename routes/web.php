<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

// Ad CRUD
//
Route::group(['middleware' => ['web', 'auth']],
    function () {
        Route::get('/ad', ['as' => 'ad.index', 'uses' => 'AdController@index']);
        Route::get('/ad/add', ['as' => 'ad.add', 'uses' => 'AdController@add']);
        Route::get('/ad/{id}', ['as' => 'ad.edit', 'uses' => 'AdController@edit']);
        Route::post('/ad/save', ['as' => 'ad.save', 'uses' => 'AdController@save']);
        Route::patch('/ad/{id}', ['as' => 'ad.update', 'uses' => 'AdController@update']);
        Route::delete('/ad/{id}', ['as' => 'ad.destroy', 'uses' => 'AdController@destroy']);
    });

// Front
//
Route::group([],
    function () {
        Route::get('/', ['as' => 'index', 'uses' => 'HomeController@index']);
        Route::get('/{id}', ['as' => 'showAd', 'uses' => 'HomeController@showAd']);
    });
